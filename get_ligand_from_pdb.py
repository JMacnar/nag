import sys, math
sys.path.append('../bioshell/bin')
sys.path.append('../bioshell/data')


from pybioshell.core.data.io import find_pdb, Pdb, write_pdb

"""
WARNING!!!
This script is not working, it only contains a fragmentary code for ligand pdb extraction with pybioshell

""




#pdb = find_pdb(sys.argv[1], "./")
reader = Pdb('/med/pdbrepo/pdb_archive_unzipped/data/structures/all/pdb/pdb16pk.ent',"is_not_water")
strctr = reader.create_structure(0)
for i_chain in range(strctr.count_chains()) :
  chain = strctr[i_chain]
  for i_res in range(chain.count_residues()) :
    if chain[i_res].residue_type().code3 == sys.argv[2]:
      atoms = []
      for atom in range(chain[i_res].count_atoms()):
          print( chain[i_res][atom].to_pdb_line())
          atoms.append(chain[i_res][atom].to_pdb_line())
      ligand_pdb = '\n'.join(atoms)
      print(ligand_pdb)




      for at_name in sys.argv[3:] :
        try :
          at_name_fixed = at_name.replace("_"," " )
          atoms.append( chain[i_res].find_atom(at_name_fixed))
          if not atoms[-1] :
            sys.stderr.write("Can't find atom "+at_name_fixed+" in "+sys.argv[2]+" residue\n")
        except :
          sys.stderr.write("Can't find atom "+at_name+" in "+sys.argv[2]+" residue\n")

