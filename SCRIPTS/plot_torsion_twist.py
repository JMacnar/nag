import seaborn as sns
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# def hexbin(x, y, color, max_series=None, min_series=None, **kwargs):
#    cmap = sns.light_palette(color, as_cmap=True)
#    ax = plt.gca()
#    xmin, xmax = min_series[x.name], max_series[x.name]
#    ymin, ymax = min_series[y.name], max_series[y.name]
#    plt.hexbin(x, y, gridsize=15, cmap=cmap, extent=[xmin, xmax, ymin, ymax], **kwargs)


def hexbin(x, y, color, **kwargs):
    cmap = sns.light_palette(color, as_cmap=True)
    plt.hexbin(x, y, gridsize=50, cmap=cmap, **kwargs)

sns.set_context("poster")
sns.set(style="white")

data_file = "../INPUTS/nag/NAG_M_data.tsv"
data = pd.read_csv(data_file, sep=' ')
data.head(n=3)
data['conformation'] = data.first_wing_angle * data.second_wing_angle

# g = sns.PairGrid(data,hue = 'ligand', vars = ['r1', 'r2', 'r3'], diag_sharey=False)
# g.map_lower(sns.kdeplot)
# g.map_upper(sns.scatterplot)

# data['first_wing_angle']*data['second_wing_angle'


# g = sns.PairGrid(data, hue= 'ligand', vars = ['conformation','twist_angle'], diag_sharey=False)
# g.map_diag(plt.hist, histtype="step", lw=3)
# g = plt.hexbin( min_series=data.min(), max_series=data.max(), alpha = 0.5)

with sns.axes_style("dark"):
    sns.set_context("poster")
    g = sns.FacetGrid(data, hue="ligand", col="ligand", height=4)
g.map(hexbin, "conformation", "twist_angle", alpha=0.7)

# g.map_lower(sns.kdeplot)


# g.add_legend()


g.savefig('Torsion_twist.svg')
plt.show()
