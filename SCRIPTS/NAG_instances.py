#!/usr/bin/env python

"""
This script is for HEPES project. It finds 6-membered rings

"""

import math
import sys
from io import StringIO

sys.path.append('../bioshell/bin/')
from pybioshell.core.calc.structural import SaturatedRing6Geometry, evaluate_dihedral_angle  # , SelectResidueByName

import numpy as np
import psycopg2
from psycopg2 import extras
from rdkit import Chem
from rdkit.Chem import AllChem
from get_pdb import get_pdb_from_database, get_statistics_from_database, get_r_info
import os

sys.path.append('../bioshell/bin')
sys.path.append('../bioshell/data')

from pybioshell.core.data.io import Pdb

Chem.WrapLogs()

"""
Connect with pdbmonomers and fetch all ligands with rings
"""


# Errrorrs = []

def fetch_ligands_with_rings():
    # global connection, curs, rows, error
    try:

        connection = psycopg2.connect(database='pdbmonomers', port=55543)

        curs = connection.cursor(cursor_factory=psycopg2.extras.DictCursor)

        curs.execute("""
    select pdbid, mol_send(molecule) as molecule, atoms, rings, aromatic_rings, weight from ligands where rings = 1 and pdbid = 'NAG';
    """)
        # curs.execute("""
        # select * from matching_connectivity where pdbid1 in %s or pdbid2 in %s;
        # """, (idlist, idlist))

        rows = curs.fetchall()
        print(rows)

    except (Exception, psycopg2.Error) as error:
        print("Error while connecting to PostgreSQL pdbmonomers", error)
        # Errrorrs.append(error)

    finally:
        # closing database connection.
        if connection:
            curs.close()
            connection.close()
            print("PostgreSQL pdbmonomers connection is closed")
    return rows


def check_if_byte_has_ring(molecule, pattern):
    ring_matches = molecule.GetSubstructMatches(pattern, uniquify=False, useChirality=False)
    if len(ring_matches) > 0:
        return set(tuple(sorted(l)) for l in ring_matches)
    else:
        return False


def average(lst):
    return sum(lst) / len(lst)


def analize_ligand(ligand_id, category):
    rows_pdbfiles = get_pdb_from_database(ligand_id)
    f = open("NAG_deposit_list_single_ring.txt", "a")
    for ele in rows_pdbfiles:
        print(ele[0], ligand_id)
        get_statistics_from_database(ligand_id, ele[0], category)
        f.write(ligand_id+' ')
        f.write(str(ele) + '\n')
        get_r_info(ligand_id, ele[0], category)
    f.close()

    pdb_ids = map(lambda x: x[0], rows_pdbfiles)



if __name__ == '__main__':
    # Parameters for rings' geometrical properties statistics

    ref_bonds = np.array(
        [1.5038421059439717412, 1.4860620225108371848, 1.457898143189022667, 1.459797191904067198, 1.489131268778881026,
         1.505035492916695308])  # calculated using ~/HEPES/calculate_bonds.py, are in file ~/HEPES/reference_bonds_EPE

    factor = 180.0 / np.pi  # 3.141592653589793
    sigma_EPE = 9.47439  # sigma of twisted angle, calculated using awk after run ./validate_saturated_ring6.py on whole PDB  9.4334
    sigma_MES = 9.4334   # sigma of twisted angle, calculated using awk after run ./validate_saturated_ring6.py on whole PDB  9.11488

    checked = set()
    all_mes_rings = 0
    all_epe_rings = 0

    """
    Check rings in ligands form pdbmonomers database
    """
    # Six-membered ring
    patt = Chem.MolFromSmarts(
        '[*]~1~[*]~[*]~[*]~[*]~[*]~1')  # [*]-*~1~[#6]~[#6]~*~[#6]~[#6]~1') #[Cc]-1-[Cc]-*-[Cc]-[Cc]-*-1')
    patt_arom1 = Chem.MolFromSmarts('[a]:1:[a]:[a]:[a]:[a]:[a]:1')
    patt_arom2 = Chem.MolFromSmarts(
        '*-1=*-*=*-*=*-1')  # only CLP has patt_arom2 and 3 - its build from Fe and S, without carbon
    patt_arom3 = Chem.MolFromSmarts('*=1-*=*-*=*-*=1')
    patt_ali = Chem.MolFromSmarts('[A]-1-[A]-[A]-[A]-[A]-[A]-1')
    patt_mes = Chem.MolFromSmarts('[*]-[*]-1-[#6]-[#6]-[*]-[#6]-[#6]-1')
    patt_epe = Chem.MolFromSmarts('[*]-*-1-[#6]-[#6]-*(-[*])-[#6]-[#6]-1')

    pdb_path = "/med/pdbrepo/pdb_archive/data/structures/all/pdb/pdb"

    if os.path.exists("demofile.txt"):
        os.remove("demofile.txt")
        sample = open('demofile.txt', 'w')
    else:
        sample = open('demofile.txt', 'w')

    if os.path.exists("*statistics.txt"):
        os.remove("*_statistics.txt")
#        statistix = open('statistics.txt', 'w')
#    else:
#        statistix = open('statistics.txt', 'w')

    rows = fetch_ligands_with_rings()

    for row in rows:
        # print(row[0])
        matching = False
        arom_ring = 0
        ali_ring = 0
        mes_ring = 0
        epe_ring = 0

        ligand_code = row[0]
        print(ligand_code)
        # ligand_pdb_out = ligand_code + '_mes.pdb'

        m = Chem.Mol(row[1].tobytes())  # Chem.MolFromSmiles(row[1])
        m1_smiles = Chem.MolToSmiles(m, True)
        #    all_matches = m.GetSubstructMatches(patt, uniquify=False, useChirality=False)

        # Check if there is any 6-member ring
        if check_if_byte_has_ring(m, patt):

            # Declare aromatic ring patterns and check if ligand has any
            arom_ring = check_if_byte_has_ring(m, patt_arom1)

            # Declare 6-member aliphatic ring and check if ligand has any
            if check_if_byte_has_ring(m, patt_ali):
                # Check if has MES-like rings
                if check_if_byte_has_ring(m, patt_mes):
                    # Check if has EPE-like rings
                    if check_if_byte_has_ring(m, patt_epe):
                        # AllChem.MolToPDBFile(m, ligand_pdb_out)
                        ligand_code = row[0]
                        # print(ligand_code)
                        # If has EPE-like ring try to find in metapdb database PDBids of pdb files containing this ligand
                        analize_ligand(ligand_code, 'E')
                    else:
                        ligand_code = row[0]
                        # print(ligand_code)
                        # If has EPE-like ring try to find in metapdb database PDBids of pdb files containing this ligand
                        analize_ligand(ligand_code, 'M')
                        # f = open("ligand_deposit_list_MES_1ring.txt", "a")
                        # for ele in rows_pdbfiles:
                        #    f.write(ligand_code+' ')
                        #    f.write(str(ele) + '\n')
                        # f.close()

    # for i_ring in epe_ring: print(i_ring) for atom in i_ring: print(row[0], m.GetAtomWithIdx(atom).GetSymbol(),
    # m.GetBondWithIdx(atom).GetEndAtomIdx()) print(row, ali_ring, arom_ring, mes_ring, epe_ring)

    print("DONE")
