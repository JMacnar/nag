import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats

#data_url = 'http://bit.ly/2cLzoxH'
data_file = "all_data_EPE_MES"
#data1_file = "EPE_ang_bond_db" # Output from the test
#data2_file = "MES_ang_bond_db" # Output from the test
data = pd.read_csv(data_file, sep=' ')
#data1 = pd.read_csv(data1_file, sep=' ')
#data2 = pd.read_csv(data2_file, sep=' ')
data.head(n=3)
#data1.head(n=3)
#data2.head(n=3)

#data['resolution_bin']=21
#print(data)


#bins = [0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5]
#names = [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5]
#data['resolution_bin'] = pd.cut(data.resolution, bins, labels=names)
#print(data)

bins = [0.0, 2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0, 20.0, 22.0, 24.0, 26.0, 28.0, 30.0, 32.0, 34.0]
names = [2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0, 20.0, 22.0, 24.0, 26.0, 28.0, 30.0, 32.0, 34.0]
data['twist_bin'] = pd.cut(data.twist_angle, bins)
        

sns.set(style="whitegrid")

g = sns.catplot(x="twist_bin", y="avg_bonds_err", hue="ligand", data=data,
        height=10, aspect=2.0, kind="swarm", palette="muted" )
g.despine(left=True)
g.set_ylabels("bond length error [A]")
g.set_xlabels("twist_angle [deg]")


plt.savefig('Twist_bond_err.svg')


plt.show()
# Generate some sequential data







#X = []
#Y = []
#for line in open(input_data_file) :
#  if line[0] =='#' : continue
#  tokens = line.strip().split()
#  X.append(float(tokens[1]))
#  Y.append(float(tokens[1]))

