import seaborn as sns
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def hexbin(x, y, color, max_series=None, min_series=None, **kwargs):
    cmap = sns.light_palette(color, as_cmap=True)
    ax = plt.gca()
    xmin, xmax = min_series[x.name], max_series[x.name]
    ymin, ymax = min_series[y.name], max_series[y.name]
    # plt.yscale('log')
    # plt.xscale('log')
    plt.hexbin(x, y, gridsize=25, cmap=cmap, extent=[-180, 180, -180, 90],
               **kwargs)  # [xmin, xmax, ymin, ymax], **kwargs)


sns.set(style="white")

# data_file = "all_data_EPE_MES"
# data = pd.read_csv(data_file, sep=' ')
# data.head(n=3)


# ligand res_no PDBid type t1 t2 t3 twist_angle conf first_wing_angle second_wing_angle deposition_year update_year resolution R_free R_work R_obs


data_file = "../INPUTS/nag/NAG_M_data.tsv"
# data1_file = "EPE_ang_bond_db" # Output from the test
# data2_file = "MES_ang_bond_db" # Output from the test
data = pd.read_csv(data_file, sep=' ',
                   dtype={"res_no": "str", "ligand": "str", "resolution": float, "R_free": float, "R_work": float,
                          "R_obs": float})
data.head(n=3)
print(data['resolution'])
# g = sns.PairGrid(data,hue = 'ligand', vars = ['r1', 'r2', 'r3'], diag_sharey=False)
# g.map_lower(sns.kdeplot)
# g.map_upper(sns.scatterplot)

sns.set_context("poster")
g = sns.PairGrid(data, hue='ligand', vars=['t1', 't2', 't3'], diag_sharey=False)
g.map_diag(plt.hist, histtype="step", lw=3)
g.map_upper(sns.scatterplot, alpha=0.5)  # hexbin, min_series=data.min(), max_series=data.max(), alpha=0.5)
g.map_lower(sns.kdeplot, levels=50, alpha=0.5)
g.axes[0, 0].set_xlim(-180, 180)
g.axes[0, 1].set_xlim(-180, 180)
g.axes[0, 2].set_xlim(-180, 180)
g.axes[0, 0].set_ylim(-180, 180)
g.axes[1, 0].set_ylim(-180, 180)
g.axes[2, 0].set_ylim(-180, 180)

# data1_file = "EPE_ang_bond_db" # Output from the test
# data2_file = "MES_ang_bond_db" # Output from the test
# g.add_legend()


g.savefig('Torsion.svg')
plt.show()
