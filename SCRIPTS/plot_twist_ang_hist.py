import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats


#data1_file = "out_EPE" 
#data2_file = "out_MES"
#data1 = pd.read_csv(data1_file, sep=' ')
#data2 = pd.read_csv(data2_file, sep=' ')
data_file = "../INPUTS/nag/NAG_M_data.tsv"
data = pd.read_csv(data_file, sep=' ')
data.head(n=3)

#gapminder['lifeExp'].hist(bins=100, grid=False, xlabelsize=12, ylabelsize=12)
#plt.xlabel("Life Expectancy", fontsize=15)
#plt.ylabel("Frequency",fontsize=15)
#plt.xlim([22.0,90.0])

#sns.distplot(gapminder['Twist_angle'],norm_hist=True, kde=False, color='blue')

bins = [0.0, 2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0, 20.0, 22.0, 24.0, 26.0, 28.0, 30.0, 32.0, 34.0]
names = [2.0, 4.0, 6.0, 8.0, 10.0, 12.0, 14.0, 16.0, 18.0, 20.0, 22.0, 24.0, 26.0, 28.0, 30.0, 32.0, 34.0]
data['twist_bin'] = pd.cut(data.twist_angle, bins)
x, y, hue = "twist_bin", "prop", "ligand"

sns.set(style="whitegrid")
sns.set_context("poster")

plt.figure(figsize=(20, 15))
plt.xlabel('Twist angle [deg]')
plt.ylabel('Frequency')
plt.xticks(rotation=30)
prop_df = (data[x]
           .groupby(data[hue])
           .value_counts(normalize=True)
           .rename(y)
           .reset_index())

h = sns.barplot(x=x, y=y, hue=hue, data=prop_df)
h.set(xlabel="Twist angle [deg]", ylabel='Frequency')

#g = sns.catplot(y="twist_bin", hue="ligand", data=data,
#        height=10, aspect=2.0, kind="count", palette="muted" )

#g.set_xlabels("twist angle [deg]")
#g.set_ylabels("Frequency")
#g.despine(left=True)




#colors = ["#6495ED", "#F08080"]
#data = [data1['Twist_angle'], data2['Twist_angle']]
#plt.hist(data, bins, density=True, histtype='bar', color=colors, alpha=.5, label=colors)


#plt.hist(data1['Twist_angle'], bins, density=True, histtype='bar', color="#6495ED", alpha=.5)
#plt.hist(data2['Twist_angle'], bins, density=True, histtype='bar', color="#F08080", alpha=.5);


#plt.hist(data['twist_angle'], 6, color=sns.desaturate("indianred", .75));

#plt.title('', fontsize=18)


plt.savefig('Twist_deg.svg')

plt.show()
# Generate some sequential data







#X = []
#Y = []
#for line in open(input_data_file) :
#  if line[0] =='#' : continue
#  tokens = line.strip().split()
#  X.append(float(tokens[1]))
#  Y.append(float(tokens[1]))

