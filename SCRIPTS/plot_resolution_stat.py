import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats

#data_url = 'http://bit.ly/2cLzoxH'
data_file = "../INPUTS/nag/NAG_M_data.tsv"
#data1_file = "EPE_ang_bond_db" # Output from the test
#data2_file = "MES_ang_bond_db" # Output from the test
data = pd.read_csv(data_file, sep=' ')
#data1 = pd.read_csv(data1_file, sep=' ')
#data2 = pd.read_csv(data2_file, sep=' ')
data.head(n=3)
#data1.head(n=3)
#data2.head(n=3)

#data['resolution_bin']=21
#print(data)

# ligand res_no PDBid type t1 t2 t3 twist_angle conf first_wing_angle second_wing_angle deposition_year update_year resolution R_free R_work R_obs
bins = [0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5]
names = [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5]
data['resolution_bin'] = pd.cut(data.resolution, bins, labels=names)
#print(data)


        

sns.set(style="whitegrid")

g = sns.catplot(x="resolution_bin", y="avg_bonds_err", hue="ligand", data=data,
        height=10, aspect=2.0, kind="swarm", palette="muted" )
g.despine(left=True)
g.set_ylabels("bond length error [A]")
g.set_xlabels("resolution [A]")


plt.savefig('Bond_resolution.svg')


plt.show()
# Generate some sequential data







#X = []
#Y = []
#for line in open(input_data_file) :
#  if line[0] =='#' : continue
#  tokens = line.strip().split()
#  X.append(float(tokens[1]))
#  Y.append(float(tokens[1]))

