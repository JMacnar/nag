import math, sys
import numpy as np
sys.path.append('/home/jmacnar/src.git/epe-like-validation/')
from pybioshell.core.chemical import PdbMolecule
from pybioshell.core.chemical import find_rings
from pybioshell.core.data.structural import SelectResidueByName
from pybioshell.core.calc.structural import SaturatedRing6Geometry

to_deg = 180.0 / math.pi


def load_atoms_counts(fname):

    f = open(fname)
    for i in range(3): f.readline()
    tokens = f.readline().strip().split()
    n = int(tokens[0][0:3])
    n_atoms = 0
    for i in range(n):
        line = f.readline()
        try:
            if line[31] != 'H':
                n_atoms += 1
        except Exception as error:
                print("Some error occured", line, error)

    return n_atoms

def dihedral(p):
    """https://stackoverflow.com/questions/20305272/dihedral-torsion-angle-from-four-points-in-cartesian-coordinates-in-python"""
    b = p[:-1] - p[1:]
    b[0] *= -1
    v = np.array( [ v - (v.dot(b[1])/b[1].dot(b[1])) * b[1] for v in [b[0], b[2]] ] )
    # Normalize vectors
    v /= np.sqrt(np.einsum('...i,...i', v, v)).reshape(-1,1)
    b1 = b[1] / np.linalg.norm(b[1])
    x = np.dot(v[0], v[1])
    m = np.cross(v[0], b1)
    y = np.dot(m, v[1])
    return np.degrees(np.arctan2( y, x ))

def average(lst):
    return sum(lst) / len(lst)

PATH_TO_IDEAL_SDF = "../ideal/"
if __name__ == "__main__":

    ref_bonds = np.array([1.531, 1.532, 1.532, 1.531, 1.430, 1.429])
    ref_torsion = np.array([-56.996, 57.030, -57.617])
    ref_twist = np.array([3.137, -29.872, 29.209])
    fname = sys.argv[1]
    code = sys.argv[2]
    n_atoms = load_atoms_counts(PATH_TO_IDEAL_SDF + code + "_ideal.sdf")
    filter_ligand = SelectResidueByName(code)
    mol = PdbMolecule.from_pdb(fname, filter_ligand)
    if mol.count_atoms() != n_atoms:
        atoms_names = set()
        for i in range(mol.count_atoms()):
            atoms_names.add(mol.get_atom(i).atom_name())
        print(sorted(atoms_names), "Too few atoms in %s! Expected: %d Found: %d" % (fname, n_atoms, mol.count_atoms()), file=sys.stderr)
        sys.exit(0)

    rings = find_rings(mol)
    if len(rings) == 0:
        print("No rings found in", fname, file=sys.stderr)
    for ring in rings:
        if len(ring) != 6:
            print("Ring length differs from 6", fname, file=sys.stderr)
            sys.exit(0)
        atoms = []
        for atom_i in ring:
            atom = mol.get_atom(atom_i)
            # print(atom.atom_name())
            atoms.append(atom)
        bonds = []
        bonds_err = []
        # print(dir(r))
        for i in range(len(atoms)):
            try:
                bond = atoms[i].distance_to(atoms[i + 1])
                bonds.append(bond)
                bonds_err.append(bond - ref_bonds[i])
            except:
                bond = atoms[i].distance_to(atoms[0])
                bonds.append(bond)
                bonds_err.append(bond - ref_bonds[-1])
        #print(
       #     "{:4s} {:2s} {:3s} {:3s} {:3.3f} {:3.3f} {:3.3f} {:5s} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:1s}".format(
       #         pdb_id, chain.id(), chain[i_res].residue_id(), ligand_id, s.first_wing_angle() * factor,
       #                                                                   s.second_wing_angle() * factor,
       #                                                                   s.twist_angle() * factor,
       #         conf,
       #                                                                   r1 * factor, r2 * factor,
       #                                                                   r3 * factor, *bonds,
       #         average(bonds_err), flag), file=sample)
       # print(
       #     "{:4s} {:2s} {:3s} {:3s} {:3.3f} {:3.3f} {:3.3f} {:5s} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:1s}".format(
       #         pdb_id, chain.id(), chain[i_res].residue_id(), ligand_id, s.first_wing_angle() * factor,
       #                                                                   s.second_wing_angle() * factor,
       #                                                                   s.twist_angle() * factor,
       #         conf,
       #                                                                   r1 * factor, r2 * factor,
       #                                                                   r3 * factor, *bonds,
       #         average(bonds_err), flag)) 
        points = [np.array([atoms[i].x, atoms[i].y,atoms[i].z]) for i in range(6)]
        t1 = dihedral(np.array(points[0:4]))
        t2 = dihedral(np.array(points[1:5]))
        t3 = dihedral(np.array(points[2:6]))
        g = SaturatedRing6Geometry(atoms[0], atoms[1], atoms[2], atoms[3], atoms[4], atoms[5])
        print("%20s %8.3f %8.3f %8.3f  %8.3f %8.3f %8.3f  %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f %8.3f" %(fname, t1, t2, t3, g.twist_angle()*to_deg, g.first_wing_angle()*to_deg, g.second_wing_angle()*to_deg, *bonds, average(bonds_err)))
