import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from scipy import stats

print(sns.__version__)

def hexbin(x, y, color, **kwargs):
    cmap = sns.light_palette(color, as_cmap=True)
    plt.hexbin(x, y, gridsize=50, cmap=cmap, **kwargs)


# data_url = 'http://bit.ly/2cLzoxH'
data_file = "../INPUTS/nag/NAG_M_data.tsv"
# data1_file = "EPE_ang_bond_db" # Output from the test
# data2_file = "MES_ang_bond_db" # Output from the test
data = pd.read_csv(data_file, sep=' ',
                   dtype={"res_no": "str", "ligand": "str", "resolution": float, "deposition_year": int,
                          "update_year": int, "R_free": float, "R_work": float,
                          "R_obs": float})
# data1 = pd.read_csv(data1_file, sep=' ')
# data2 = pd.read_csv(data2_file, sep=' ')
data.head(n=3)


def label_year(row):
    if row["deposition_year"] < 2010:
        return '1999-2009'
    else:
        return '2010-2020'


data['Decades'] = data.apply(lambda row: label_year(row), axis=1)



# data1.head(n=3)
# data2.head(n=3)


# gapminder['lifeExp'].hist(bins=100, grid=False, xlabelsize=12, ylabelsize=12)
# plt.xlabel("Life Expectancy", fontsize=15)
# plt.ylabel("Frequency",fontsize=15)
# plt.xlim([22.0,90.0])

# sns.distplot(gapminder['Twist_angle'],norm_hist=True, kde=False, color='blue')

# max_data = int(np.r_[data1['Twist_angle'], data2['Twist_angle']].max())
# bins = np.linspace(0, max_data, max_data + 1)
# colors = ["#6495ED", "#F08080"]
# data = [data1['Twist_angle'], data2['Twist_angle']]
# plt.hist(data, bins, density=True, histtype='bar', color=colors, alpha=.5, label=colors)


# plt.hist(data1['Twist_angle'], bins, density=True, histtype='bar', color="#6495ED", alpha=.5)
# plt.hist(data2['Twist_angle'], bins, density=True, histtype='bar', color="#F08080", alpha=.5);


# plt.hist(data['Twist_angle'], 6, color=sns.desaturate("indianred", .75));

# plt.title('', fontsize=18)
# plt.xlabel('Twist angle [deg]', fontsize=16)
# plt.ylabel('Frequency', fontsize=16)

# f = plt.figure(figsize=(100,5))

# ax = f.subplot()
sns.set(style="whitegrid")
sns.set_context("poster")
g = sns.catplot(x="update_year", y="avg_bonds_err", hue="type", data=data,
                height=10, aspect=2.0, kind="swarm", palette="muted", s=7)
g.despine(left=True)
g.set_ylabels("Bonds length error [A]")
g.set_xlabels("Update year")
g.set_xticklabels(rotation=30)

g.savefig('Bond_update_year_class.svg')

plt.show()
# Generate some sequential data
# f = sns.catplot(x="deposition_year", y="avg_bonds_err", hue="method", data=data,
#        height=10, aspect=2.0, kind="swarm", palette="muted" )
# f.despine(left=True)
# f.set_ylabels("bond length error [A]")

# plt.savefig('Bond_year_method.svg')
# plt.show()

bins = [1.0, 1.5, 2.0, 2.5, 3.0, 3.5]
names = [1.5, 2.0, 2.5, 3.0, 3.5]
data['resolution_bin'] = pd.cut(data.resolution, bins, labels=names)
# print(data)


e = sns.catplot(x="resolution_bin", y="avg_bonds_err", hue="type", data=data,
                height=10, aspect=2.0, kind="swarm", palette="muted", s=7)
e.despine(left=True)
e.set_ylabels("Bonds length error [A]")
e.set_xlabels("Resolution [A]")

e.savefig('Bond_resolution_class.svg')

plt.show()

data['conformation'] = data.first_wing_angle * data.second_wing_angle
print(data["first_wing_angle"], data["second_wing_angle"], data["conformation"])
# g = sns.PairGrid(data,hue = 'ligand', vars = ['r1', 'r2', 'r3'], diag_sharey=False)
# g.map_lower(sns.kdeplot)
# g.map_upper(sns.scatterplot)

# data['first_wing_angle']*data['second_wing_angle'


# g = sns.PairGrid(data, hue= 'ligand', vars = ['conformation','twist_angle'], diag_sharey=False)
# g.map_diag(plt.hist, histtype="step", lw=3)
# g = plt.hexbin( min_series=data.min(), max_series=data.max(), alpha = 0.5)


# with sns.axes_style("dark"):
#    h = sns.FacetGrid(data, hue="ligand", col="type", height=4)
# h.map(hexbin, "conformation", "twist_angle", alpha=0.5)
# plt.savefig('Twist_torsion_conformations.svg')

sns.set(style="white")
sns.set_context("poster")
j = sns.jointplot(data=data, x="conformation", y="twist_angle", hue='Decades', alpha=0.5, height=10)
j.plot_joint(sns.kdeplot, hue='Decades', color="r", zorder=0, levels=6)
j.plot_marginals(sns.rugplot, hue='Decades', color="r", height=-.15, clip_on=False)
j.set_axis_labels("Conformation", "Twist angle [deg]")
j.ax_marg_x.set_xlim(-1350, 1000)
j.savefig('Twist_torsion_conformations_.svg')
plt.show()

k = sns.jointplot(data=data, x="conformation", y="twist_angle", kind="hex")
k.set_axis_labels("Conformation", "Twist angle [deg]")
# k.ax_marg_x.set_xlim(-1200, 1000)
k.savefig('Twist_torsion_conformations__1.svg')
plt.show()

l = sns.jointplot(data=data, x="conformation", y="twist_angle", kind="kde", hue='Decades')
l.set_axis_labels("Conformation", "Twist angle [deg]")
l.ax_marg_x.set_xlim(-1350, 1000)
l.savefig('Twist_torsion_conformations___.svg')
plt.show()

# X = []
# Y = []
# for line in open(input_data_file) :
#  if line[0] =='#' : continue
#  tokens = line.strip().split()
#  X.append(float(tokens[1]))
#  Y.append(float(tokens[1]))
